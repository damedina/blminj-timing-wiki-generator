import cache as c

class LtimType:
    SBP = 'SBP'
    SBIN = 'SBIN'
    EBOUT = 'EBOUT'
    SCY = 'SCY'

class Ltim:
    def __init__(self, fec_name, ltim_type):
        self.ltim_type = ltim_type
        self.fec_name = fec_name
        instance_file_root = c.get_ltim_instance_file_root(self.fec_name)
        persistency_file_root = c.get_ltim_persistency_file_root(self.fec_name)
        all_devices_in_instance = [x for x in instance_file_root.findall(".//device-instance")]
        node_ltim_instance = filter(lambda d: d.attrib.get('name').find(ltim_type) != -1, all_devices_in_instance)[0]
        self.eqp_nb = node_ltim_instance.find(".//configuration/eqpNb/value").text
        self.name = node_ltim_instance.attrib.get('name')
        self.load_event = node_ltim_instance.find('.//loadEvent/value').text
        self.channel = node_ltim_instance.find('.//addr').attrib.get('channel')
        self.delay = persistency_file_root.find(".//device-instance[@name='{}']/delay/cycle".format(self.name)).attrib.get('value')
        self.clock = persistency_file_root.find(".//device-instance[@name='{}']/clock/cycle".format(self.name)).attrib.get('value')
        self.omask = persistency_file_root.find(".//device-instance[@name='{}']/omask/cycle".format(self.name)).attrib.get('value')
        self.start = persistency_file_root.find(".//device-instance[@name='{}']/start/cycle".format(self.name)).attrib.get('value')
class bcolors:
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'


acc_names = {"SPS Ring": "SPS", "LINAC4" : "LN4", "TT10" : "TT10", "TT2" : "TT2","PSB Ring" : "PSB", "PS Ring": "PS","BE_BT":"PSB" ,"BT" : "PSB", "BI" : "PSB", "LAB" : "LAB", "F16" : "PS"}
all_crates = ["cfv-269-blmtt2", "cfv-400-blmln4a", "cfv-400-blmln4b", "cfv-361-blmbi", "cfv-361-blmbra", "cfv-361-blmbrb", "cfv-361-blmbt", "cfv-269-blmtt10a", \
             "cfv-ba1-blmtt10b", "cfv-865-blm5", "cfv-359-blmpra", "cfv-359-blmprb", "cfv-157-blmea"]
lab_crates = ["cfv-865-blm5"]
c1KHZ = (1, "ms")
c10MHZ = (100, "ns")
c40MHZ = (25, "ns")
clock_type = {"_1KHZ": c1KHZ, "_10MHZ" : c10MHZ, "_40MHZ": c40MHZ, "Clock1KHZ": c1KHZ, "Clock10MHZ": c10MHZ, "Clock40MHZ":c40MHZ}
clock_type_str = {"_1KHZ": "1KHZ", "_10MHZ" : "10MHZ", "_40MHZ": "40MHZ", "Clock1KHZ": "1KHZ", "Clock10MHZ": "10MHZ", "Clock40MHZ":"40MHZ", "?":"?"}

