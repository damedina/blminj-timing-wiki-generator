#!/usr/bin/env python

# This script generates ALL the content for the following wiki page: https://wikis.cern.ch/pages/editpage.action?pageId=126527062
# To do so it creates a snapshot of the current state of the system (cache) by copying all FECS instance files and LTIM persistency to your /tmp folder
# (It might take a while :)). An option is provided to use an already existing cache. Some extra options are provided to run different checks on 
# LTIM configurations 
# To use:
#./wikigen.py --print-wiki > /tmp/BLMINJTimingTables
# then copy the content of the file and paste in the wiki: https://wikis.cern.ch/pages/editpage.action?pageId=126527062

import argparse
import ltimchecks as ltimchecks
import cache as cache
import table as table
import sys

def print_help_and_exit_if_no_args():
    if len(sys.argv) < 2:
        parser.print_usage()
        sys.exit(1)

parser = argparse.ArgumentParser()
parser.add_argument('--print-wiki', action='store_true')
parser.add_argument('--cached', action='store_true')
parser.add_argument('--check-ltims', action='store_true')
parser.add_argument('--check-eqp-nb', action='store_true')


print_help_and_exit_if_no_args()

args = parser.parse_args()

if not args.cached:
    cache.create_cache()
if args.check_eqp_nb:
    ltimchecks.check_eqp_nb()
if args.check_ltims:
    ltimchecks.checkltims()
if args.print_wiki:
    t = table.TimingTable(cache.get_all_ltim_instance_element_trees())
    t.print_timing_table_header()
    t.print_timing_table_body()

    t.print_special_timing_cases()

    t.print_event_table_header()
    t.print_event_table_body()

    t.print_timing_image()        


