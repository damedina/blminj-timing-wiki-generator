# Blminj Timing Wiki Generator

This script generates the content for the following wiki page: https://wikis.cern.ch/pages/editpage.action?pageId=126527062

To do so it creates a snapshot of the current state of the system (cache) by copying all FEC instance files, LTIM persistency and FEC signal configuration (devices using EXT start) to your /tmp folder (It might take a while :)).

An option is provided to use an already existing cache. Some extra options are provided to run different checks on LTIM configurations
(Use --help for more info!)

To use:
./wikigen.py --print-wiki > /tmp/BLMINJTimingTables
then copy the content of the file and paste in the wiki: https://wikis.cern.ch/pages/editpage.action?pageId=126527062

