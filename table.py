import blminjdefinitions as bdef
import cache as cache
class TimingTableRow:
    def __init__(self, machine, fec_name, ltim_sbp, ltim_sbin, ltim_ebout, ltim_scy):
        self.machine = machine
        self.fec_name = fec_name
        self.ltim_sbp = ltim_sbp
        self.ltim_sbin = ltim_sbin
        self.ltim_ebout = ltim_ebout
        self.ltim_scy = ltim_scy

class TimingTable:
    table_content = {}
    def __init__(self, roots_ltim):
        for r in roots_ltim:
            fec_name = r.find(".//fec-name").text
            accelerator_zones = filter(lambda zone : zone.attrib.get('value'), r.findall('.//acceleratorZone'))
            accelerator_zones = filter(lambda zone : zone != '', accelerator_zones)
            zone = 'LAB'
            if fec_name not in bdef.lab_crates and len(accelerator_zones) > 0:
                zone = accelerator_zones[0].attrib.get('value')

            machine = bdef.acc_names[zone]

            ltim0 = bdef.Ltim(fec_name, bdef.LtimType.SBP)
            ltim1 = bdef.Ltim(fec_name, bdef.LtimType.SBIN)
            ltim2 = bdef.Ltim(fec_name, bdef.LtimType.EBOUT)
            ltim3 = bdef.Ltim(fec_name, bdef.LtimType.SCY)

            if machine not in self.table_content:
                self.table_content[machine] = []
            self.table_content[machine].append(TimingTableRow(machine, fec_name, ltim0, ltim1, ltim2, ltim3))

        for acc in self.table_content:
            sorted(self.table_content[acc], key= lambda row : row.fec_name)

    def print_timing_table_header(self):
        print """ 
        <h1>Timing</h1>
        <table class="fixed-table wrapped">
          <colgroup> <col style="width: 82.0px;"/> <col style="width: 134.0px;"/> <col style="width: 160.0px;"/> <col style="width: 110.0px;"/> <col style="width: 160.0px;"/> <col style="width: 169.0px;"/> <col style="width: 132.0px;"/> <col style="width: 210.0px;"/> <col style="width: 178.0px;"/> <col style="width: 132.0px;"/> <col style="width: 210.0px;"/> <col style="width: 160.0px;"/> <col style="width: 111.0px;"/> <col style="width: 175.0px;"/> </colgroup>
          <tbody>
            <tr>
              <td rowspan="2">
                <strong>Machine</strong>
              </td>
              <td rowspan="2">
                <strong>FEC</strong>
              </td>
              <td class="highlight-blue" colspan="3" data-highlight-colour="blue">
                <strong>LTIM Output 1 (Start Basic Period)</strong>
              </td>
              <td class="highlight-blue" colspan="3" data-highlight-colour="blue">
                <strong>LTIM Output 2 (Beam Start)</strong>
              </td>
              <td class="highlight-blue" colspan="3" data-highlight-colour="blue">
                <strong>LTIM Output 3 (Beam End)</strong>
              </td>
              <td class="highlight-blue" colspan="3" data-highlight-colour="blue">
                <strong>LTIM Output 4 (Start Cycle)</strong>
              </td>
            </tr>
            <tr>
              <td colspan="1">
                <strong>Name</strong>
              </td>
              <td colspan="1">
                <strong>Load / Start</strong>
              </td>
              <td colspan="1">
                <strong>Delay</strong>
              </td>
              <td colspan="1">
                <strong>Name</strong>
              </td>
              <td colspan="1">
                <strong>Load / Start</strong>
              </td>
              <td colspan="1">
                <strong>Delay</strong>
              </td>
              <td colspan="1">
                <strong>Name</strong>
              </td>
              <td colspan="1">
                <strong>Load / Start</strong>
              </td>
              <td colspan="1">
                <strong>Delay</strong>
              </td>
              <td colspan="1">
                <strong>Name</strong>
              </td>
              <td colspan="1">
                <strong>Load / Start</strong>
              </td>
              <td colspan="1">
                <strong>Delay</strong>
              </td>
            </tr>
        """


    def print_timing_table_body(self):
        for acc in sorted([acc for acc in self.table_content]):
            print "<tr><td rowspan=\"{}\"><strong>{}</strong></td>".format(len(self.table_content[acc]), acc)
            first_row = True
            for table_row in self.table_content[acc]:
                if not first_row:
                    print "<tr>"
                first_row = False
                print "<td>{}</td>".format(table_row.fec_name)
                for ltim in [table_row.ltim_sbp, table_row.ltim_sbin, table_row.ltim_ebout, table_row.ltim_scy]:
                    load_start = ltim.load_event
                    if ltim.start != "NORMAL":
                        load_start = ltim.load_event + " / " + cache.get_timing_signal_ext(ltim.start, ltim.fec_name)
                        
                    if False: #table_row.fec_name in ["cfv-400-blmln4b", "cfv-400-blmln4a"] and ltim.ltim_type in [bdef.LtimType.SBIN, bdef.LtimType.EBOUT]:
                        print """<td class="highlight-grey" colspan="3" data-highlight-colour="grey" style="text-align: center;">**NOT USED** (see note below)</td>"""
                    else:
                        print """ <td class="highlight-blue" data-highlight-colour="blue">{}<br/>omask:{},channel:{}</td>
              <td class="highlight-yellow" data-highlight-colour="yellow">{}</td>
              <td>{}<br/>(delay:{} clock:{})</td>""".format(ltim.name, ltim.omask, ltim.channel, load_start, \
                                                            str(int(ltim.delay) * bdef.clock_type[ltim.clock][0]) +' '+bdef.clock_type[ltim.clock][1],\
                                                            ltim.delay, bdef.clock_type_str[ltim.clock])
                print "</tr>"
        print "</tbody></table>"

    def __get_fec_rows_for_machine(self, machine):
        return self.table_content[machine]

    def __get_machines(self):
        return sorted([acc for acc in self.table_content])

    def print_event_table_body(self):
        for acc in self.__get_machines():
            print "<tr><td rowspan=\"{}\"><strong>{}</strong></td>".format(len(self.__get_fec_rows_for_machine(acc)), acc)
            first_row = True
            for table_row in self.__get_fec_rows_for_machine(acc):
                if not first_row:
                    print "<tr>"
                first_row = False
                print "<td colspan=\"1\">{}</td>".format(table_row.fec_name)
                for logical_event, hw_event in cache.get_blminj_fec_events(table_row.fec_name):
                    ev = hw_event
                    color = "yellow"
                    if hw_event == None:
                        ev = "<br/>"
                    elif hw_event.find("-CT") == -1:
                        color = "blue"


                    print "<td class=\"highlight-{}\" data-highlight-colour=\"{}\">{}</td>".format(color,color,ev)
                print "</tr>"
        print "</tbody></table>"


    def print_event_table_header(self):
        print """
        <h1>FESA Events</h1>
        <table class="fixed-table wrapped">
          <colgroup> <col style="width: 84.0px;"/> <col style="width: 140.0px;"/> <col style="width: 107.0px;"/> <col style="width: 161.0px;"/> <col style="width: 161.0px;"/> <col style="width: 180.0px;"/> <col style="width: 140.0px;"/> <col style="width: 161.0px;"/> <col style="width: 180.0px;"/> <col style="width: 170.0px;"/> <col style="width: 207.0px;"/> <col style="width: 191.0px;"/> <col style="width: 158.0px;"/> <col style="width: 181.0px;"/> <col style="width: 181.0px;"/>   <col style="width: 181.0px;"/>  <col style="width: 181.0px;"/> </colgroup>
          <tbody>
            <tr>
              <td>
                <strong>Machine</strong>
              </td>
              <td>
                <strong>FEC</strong>
              </td>
        """
        for e in cache.get_blminj_logical_event_list():
            print "<td><strong>{}</strong></td>".format(e)
        print "</tr>"

    def print_timing_image(self):

         print """
        <p class="auto-cursor-target">
          <ac:image ac:height="400">
            <ri:attachment ri:filename="image2019-4-25_15-57-42.png"/>
          </ac:image>
        </p>
        """


    def print_special_timing_cases(self):
        print """
<h1>Special cases</h1>
<p>
  <ac:link>
    <ri:page ri:content-title="BLMINJ LN4 Timing Configuration"/>
  </ac:link>
</p>
<p>
  <ac:link>
    <ri:page ri:content-title="BLMINJ TT2 Timing Configuration"/>
  </ac:link>
</p>
<p>
  <ac:link>
    <ri:page ri:content-title="BLMINJ TT10 Timing Configuration"/>
  </ac:link>
</p>
"""
