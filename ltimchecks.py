import cache as cache
import blminjdefinitions as bdef

def _repeated_eqp_nb(root):
    l = [x.text for x in root.findall(".//device-instance/configuration/eqp_nb/value")]
    return len(l)!=len(set(l))

def check_eqp_nb():
    for ltim_element_tree in cache.get_all_ltim_instance_element_trees():
        crate = ltim_element_tree.find(".//fec-name").text
        if _repeated_eqp_nb(ltim_element_tree):
            print bdef.bcolors.FAIL+"[ERROR]"+bdef.bcolors.ENDC+" Equipment nb. is repeated in {}".format(crate)
        else:
            print bdef.bcolors.OKGREEN+"[OK]"+bdef.bcolors.ENDC+" No equipment nb is repeated in {}".format(crate)


def _is_blminj_ltim(name):
    for t in [bdef.LtimType.SBP, bdef.LtimType.SBIN, bdef.LtimType.EBOUT, bdef.LtimType.SCY]:
        if t in name:
            return True
    return False

def checkltims():
    for r in cache.get_all_ltim_instance_element_trees():
        blminjLtimsWarning = ""
        otherLtimsWarning = ""

        otherLtims = filter(lambda d: not _is_blminj_ltim(d.attrib.get('name')), [x for x in r.findall(".//device-instance")])
        blminjLtims = filter(lambda d: _is_blminj_ltim(d.attrib.get('name')), [x for x in r.findall(".//device-instance")])
        fecname = r.find(".//fec-name").text

        for ll in blminjLtims:
            ltimPersistencyFileRoot = cache.get_ltim_persistency_file_root(fecname)
            name = ll.attrib.get('name')
            omask = ltimPersistencyFileRoot.find(".//device-instance[@name='{}']/omask/cycle".format(ll.attrib.get('name'))).attrib.get('value')
            outEnabled = ltimPersistencyFileRoot.find(".//device-instance[@name='{}']/outEnabled/cycle".format(ll.attrib.get('name'))).attrib.get('value')=="true"
            channel = ll.find('.//addr').attrib.get('channel')
            eqpNb = ll.find('.//eqpNb/value').text
            blminjLtimsWarning = blminjLtimsWarning + "\n    > {}(channel:{}, omask:{}, eqpNb:{})".format(name, channel, omask, eqpNb)
            if eqpNb!="10000{}".format(channel):
                blminjLtimsWarning = blminjLtimsWarning + "\n        {}[WARNING]{} eqpNb should be uniq. and by convention 10000{}".format(bdef.bcolors.WARNING,bdef.bcolors.ENDC,channel)
            if int(omask)!=2**int(channel):
                blminjLtimsWarning = blminjLtimsWarning + "\n        {}[ERROR]{} omask should be {} (2**channel)".format(bdef.bcolors.FAIL,bdef.bcolors.ENDC,2**int(channel))
            if not outEnabled:
                blminjLtimsWarning = blminjLtimsWarning + "\n        {}[ERROR]{} outputEnable == false".format(bdef.bcolors.FAIL, bdef.bcolors.ENDC)
            if len(otherLtims)!= 0:
                otherLtimsWarning = otherLtimsWarning +"\n"+ fecname
                for o in otherLtims:
                    name = o.attrib.get('name')
                    channel = o.find('.//addr').attrib.get('channel')
                    eqpNb = o.find('.//eqpNb/value').text
                    loadEvent = o.find('.//loadEvent/value').text
                    try:
                        omask = ltimPersistencyFileRoot.find(".//device-instance[@name='{}']/omask/cycle".format(o.attrib.get('name'))).attrib.get('value')  
                        clock = ltimPersistencyFileRoot.find(".//device-instance[@name='{}']/clock/cycle".format(name)).attrib.get('value')
                        delay = ltimPersistencyFileRoot.find(".//device-instance[@name='{}']/delay/cycle".format(name)).attrib.get('value')
                        busEnable = ltimPersistencyFileRoot.find(".//device-instance[@name='{}']/busEnabled/cycle".format(name)).attrib.get('value')
                        outEnable = ltimPersistencyFileRoot.find(".//device-instance[@name='{}']/outEnabled/cycle".format(name)).attrib.get('value')
                    except:
                        omask, clock, delay, busEnable, outEnable = "32", "?", "?", "?", "?"  

                    otherLtimsWarning = otherLtimsWarning + "\n    > {}(loadEvent:{}, clock:{}, delay:{}, channel:{}, omask:{}, eqpNb:{}, busEnable:{}, outEnable:{})".format(name,loadEvent,bdef.clock_type_str[clock],delay, channel, omask, eqpNb, busEnable, outEnable)
                    if eqpNb!="10000{}".format(channel):
                        otherLtimsWarning = otherLtimsWarning + "\n        {}[WARNING]{} eqpNb should be uniq. and by convention 10000{}".format(bdef.bcolors.WARNING, bdef.bcolors.ENDC, channel)
                    if int(omask)!=2**int(channel):
                        otherLtimsWarning = otherLtimsWarning + "\n        {}[ERROR]{} omask should be {} (2**channel)".format(bdef.bcolors.FAIL, bdef.bcolors.ENDC, 2**int(channel))

        print blminjLtimsWarning
        if otherLtimsWarning != "":
            otherLtimsWarning = "\n\n{}[WARNING]{} The following ltims do not match SBP/SBIN/EBOUT/SCY\n".format(bdef.bcolors.WARNING, bdef.bcolors.ENDC) + otherLtimsWarning
            print otherLtimsWarning


