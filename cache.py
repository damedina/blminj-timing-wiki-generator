import xml.etree.ElementTree as ET
import os
import blminjdefinitions as bdef

def create_cache():
    os.system('rm -rf /tmp/blminj-ltims > /dev/null 2>&1')
    os.system('rm -rf /tmp/blminj-events > /dev/null 2>&1')
    os.system('mkdir /tmp/blminj-ltims > /dev/null 2>&1')
    os.system('mkdir /tmp/blminj-events > /dev/null 2>&1')
    for c in bdef.all_crates:
        os.system("scp {}:/dsc/local/data/LTIM_DU.{}.instance /tmp/blminj-ltims/ > /dev/null 2>&1".format(c, c))
        os.system("scp {}:/dsc/local/data/LTIM_DU.{}LTIMPersistentData.xml /tmp/blminj-ltims > /dev/null 2>&1".format(c, c))
        os.system("scp {}:/dsc/local/data/BLMINJ_DU.{}.instance /tmp/blminj-events/ > /dev/null 2>&1".format(c, c))
        os.system("scp {}:/dsc/local/data/TimingFECConfig.xml /tmp/blminj-ltims/Timing{}Config.xml > /dev/null 2>&1".format(c, c))
        
    os.system("sed -i 's/outputMask/omask/g' /tmp/blminj-ltims/* > /dev/null 2>&1")

def _get_instance_files(path_to_folder):
    for (dirpath, dirname, filenames) in os.walk(path_to_folder):
        return map(lambda f: os.path.join(dirpath,f) ,list(filter(lambda s: s.endswith(".instance"), filenames)))

def get_ltim_persistency_file_root(fec_name):
    return ET.parse("/tmp/blminj-ltims/LTIM_DU.{}LTIMPersistentData.xml".format(fec_name))

def get_ltim_instance_file_root(fec_name):
    return ET.parse("/tmp/blminj-ltims/LTIM_DU.{}.instance".format(fec_name))

def get_blminj_instance_file_root(fec_name):
    return ET.parse("/tmp/blminj-events/BLMINJ_DU.{}.instance".format(fec_name))

def get_all_ltim_instance_element_trees():
    return [ET.parse(x) for x in _get_instance_files('/tmp/blminj-ltims')]

def get_all_blminj_instance_element_trees():
    return [ET.parse(x) for x in _get_instance_files('/tmp/blminj-events')]

def get_blminj_logical_event_list():
    #we use blm5 as reference for which events to take into account
    r = get_blminj_instance_file_root("cfv-865-blm5")
    events = r.findall(".//classes/BLMINJ/events-mapping/*")
    events =  filter(lambda e : e.find(".//*/hardware-event")!= None,events)
    return [e.tag for e in events]

def get_blminj_fec_events(fec_name):

    events = []
    for logical_event in get_blminj_logical_event_list():
        element_tree = get_blminj_instance_file_root(fec_name)
        try:
            hardware_event = element_tree.find(".//classes/BLMINJ/events-mapping/{}/event-configuration/*/hardware-event".format(logical_event)).attrib.get('name')
        except:
            hardware_event = None
        events.append([logical_event, hardware_event])
    return events

def get_timing_signal_ext(start, fec_name):
    if start in ["EXT1", "EXT2"]:
        element_tree = ET.parse("/tmp/blminj-ltims/Timing{}Config.xml".format(fec_name))
        start_attr = { "EXT1" : "ext_start", "EXT2" : "ext_start2" }
        return element_tree.find(".//Module[@type='CTRV']").attrib.get(start_attr[start])
    return start
